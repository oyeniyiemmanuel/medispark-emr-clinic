<?php

namespace App\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\PatientRepository;
use App\Repositories\Interfaces\PatientInterface;

class PatientServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            PatientInterface::class, 
            PatientRepository::class
        );
    }
}
