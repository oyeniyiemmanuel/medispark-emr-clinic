<?php

namespace App\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\VitalsRepository;
use App\Repositories\Interfaces\VitalsInterface;

class VitalsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            VitalsInterface::class, 
            VitalsRepository::class
        );
    }
}
