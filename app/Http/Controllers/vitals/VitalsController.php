<?php

namespace App\Http\Controllers\vitals;

use App\Repositories\Interfaces\VitalsInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;

class VitalsController extends Controller
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * declaration of vitals repository
     *
     * @var vitalsRepository
     */
    private $vitalsRepository;

    /**
     * Dependency Injection of vitalsRepository.
     *
     * @param  \App\Repositories\Interfaces\VitalsInterface  $vitalsRepository
     * @return void
     */
    public function __construct(VitalsInterface $vitalsRepository)
    {
        $this->vitalsRepository = $vitalsRepository;
    }

    /**
     * store new vitals
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newVitals(Request $request)
    {
        return $this->successResponse($this->vitalsRepository->newVitals($request));
    }
}