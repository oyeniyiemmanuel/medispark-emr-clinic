<?php

namespace App\Http\Controllers\patient;

use App\Repositories\Interfaces\PatientInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;

class PatientController extends Controller
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * declaration of Patient repository
     *
     * @var patientRepository
     */
    private $patientRepository;

    /**
     * Dependency Injection of PatientRepository.
     *
     * @param  \App\Repositories\Interfaces\PatientInterface  $patientRepository
     * @return void
     */
    public function __construct(PatientInterface $patientRepository)
    {
        $this->patientRepository = $patientRepository;
    }

    /**
     * Show all patients.
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function showAllPatients(Request $request)
    {
        return $this->successResponse($this->patientRepository->showAllPatients($request));
    }
}