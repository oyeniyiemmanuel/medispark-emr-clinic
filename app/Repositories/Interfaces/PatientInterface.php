<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\Patient;

interface PatientInterface
{
    public function showAllPatients(Request $request);
}