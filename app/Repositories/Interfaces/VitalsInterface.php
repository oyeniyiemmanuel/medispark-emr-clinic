<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\Vitals;

interface VitalsInterface
{
    public function newVitals(Request $request);
}