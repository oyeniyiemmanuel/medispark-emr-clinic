<?php

namespace App\Repositories;

use App\Repositories\Interfaces\VitalsInterface;
use Illuminate\Http\Request;
use App\Models\Vitals;

class VitalsRepository implements VitalsInterface
{
	/**
     * store new vitals
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function newVitals(Request $request)
    {
        // store new vitals details
        $new_vitals = Vitals::create([
                        "temperature" => $request->temperature,
                        "pulse" => $request->pulse,
                        "respiration" => $request->respiration,
                        "oxygen_saturation" => $request->oxygen_saturation,
                        "weight" => $request->weight,
                        "height" => $request->height,
                        "systolic_bp" => $request->systolic_bp,
                        "diastolic_bp" => $request->diastolic_bp,
                        "body_mass_index" => $request->body_mass_index,
                        "pain_score" => $request->pain_score,
                        "patient" => $request->patient,
                        "saved_by" => $request->saved_by,
                    ]);

        return $new_vitals;
    }
}