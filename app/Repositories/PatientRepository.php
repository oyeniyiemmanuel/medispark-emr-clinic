<?php

namespace App\Repositories;

use App\Repositories\Interfaces\PatientInterface;
use Illuminate\Http\Request;
use App\Models\Patient;

class PatientRepository implements PatientInterface
{
	/**
     * Show all patients.
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function showAllPatients(Request $request)
    {
        $data['message'] = "got to clinic/patients page";
        return $data;   
    }
}