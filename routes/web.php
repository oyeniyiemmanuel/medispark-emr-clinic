<?php

// routes that start with url "clinic/"
$router->group(['prefix' => 'clinic'], function () use ($router) {
    $router->get('/patients', 'patient\PatientController@showAllPatients');

    $router->post('/vitals/new', 'vitals\VitalsController@newVitals');
});
